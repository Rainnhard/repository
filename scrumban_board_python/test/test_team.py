import unittest
from collections import deque
import test as _test
from scrumban_board import team as _team
class TeamTest(_test.TestCase):
    def test_get_team_boards(self):
        self.assertEqual(type(_team.Team.get_team_boards(login="kiryapanch")), deque)
        board = _team.Board("board", "kiryapanch")
        self.assertEqual(type(_team.Team.get_team_boards(login="kiryapanch", boards=board)), deque)
    def test_get_team_members_login(self):
        self.assertEqual(type(_team.Team.get_team_members_login("kiryapanch")), deque)
if __name__ == "__main__":
    unittest.main()
