import os
import logging.config
import string
import random

from hashlib import sha1
from datetime import *
from dateutil.relativedelta import *
from scrumban_board_python.scrumban_board.terminal_colors import Colors
logging.config.fileConfig('logging.cfg')
logger = logging.getLogger("ScrumbanBoard")

class Remind(object):
    """
    Description of Remind
    Example:
    remind = scrumban_board.Remind(client.logger, "Remind", datetime.now(),)
    """
    @staticmethod
    def _get_card_id(card_id=None):
        if card_id is not None:
            return card_id
        else:
            return None
    @staticmethod
    def _get_repeating_remind_relativedelta(repeating_remind_relativedelta=None):
        if repeating_remind_relativedelta is not None:
            is_repeatable = True
        else:
            is_repeatable = False
        return repeating_remind_relativedelta, is_repeatable
    @staticmethod
    def _get_when_remind(when_remind):
        """
        Checking correct datetime input
        :param when_remind: datetime/str
        :return: datetime
        """
        if isinstance(when_remind, datetime):
            return when_remind
        elif isinstance(when_remind, str):
            try:
                return datetime.strptime(when_remind, '%Y/%m/%d %H:%M')
            except ValueError:
                try:
                    return datetime.strptime(when_remind, '%Y/%m/%d')
                except ValueError:
                    return None
        return None


    def __init__(self, logger, title: str, when_remind,
                 description: str = None, card_id: str = None,
                 repeating_remind_timedelta: timedelta = None):
        """
        Initialising of Remind
        :param logger: client logger
        :param title: remind title
        :param when_remind: when remind
        :param description: remind description
        :param card_id: card id of remind
        :param repeatable_time: if remind is periodical
        """
        self.title = title
        self.description = description


        self.card_id = Remind._get_card_id(card_id)

        self.when_remind = self._get_when_remind(when_remind)
        self.repeating_remind_relativedelta, self.is_repeatable = Remind._get_repeating_remind_relativedelta(
            repeating_remind_relativedelta)

	    self.when_remind = when_remind

            self.is_repeatable = False

	if repeating_remind_timedelta is not None:
            self.is_repeatable = True

            self.repeating_remind_relativedelta = None

	 if repeating_remind_timedelta is not None:
            if isinstance(repeating_remind_timedelta, datetime.timedelta):
                board = Board(title=board, users_login=self.team_members_id)
            elif isinstance(repeating_remind_timedelta, str):
                pass

	else:
	    self.repeating_remind_timedelta = repeating_remind_timedelta

                self.id = sha1(("Remind: " + " " +
                        self.title + " " +
                        str(self.when_remind) + " " +
                        str(datetime.now())).encode('utf-8')).hexdigest()
        self.logger.info("Remind ({}) was created".format(self.id))
    def __str__(self):
        output = Colors.remind_red + """
            --- Remind ---
            Title: {}
            Description: {}
            ID: {}
            
            When Remind: {}
            Is Repeatable: {}
            Repeating time delta: {}
            --End Remind--
""".format(self.title,
           self.description,
           self.id,
           self.when_remind,
           self.is_repeatable,
           self.repeating_remind_relativedelta) + Colors.end_color

        return output


    def __repr__(self):
        output = Colors.remind_red + """
            --- Remind ---
            Title: {}
            Description: {}
            ID: {}
            
            When Remind: {}
            Is Repeatable: {}
            Repeating time delta: {}
            --End Remind--
""".format(self.title,
           self.description,
           self.id,
           self.when_remind,
           self.is_repeatable,
           self.repeating_remind_relativedelta) + Colors.end_color

        return output




    def update_remind(self, title: str = None, description: str = None, card_id: str = None,
                      when_remind=None,
                      repeatable_time: str = None):
        """
        Updating of Remind
        :param title: remind title
        :param when_remind: when remind
        :param description: remind description
        :param card_id: card id of remind
        :param repeating_remind_relativedelta: if remind is periodical
        :return:
        """

        if title is not None:
            self.title = title

        if description is not None:
            self.description = description
        if card_id is not None:
            self.card_id = card_id
        else:
            self.card_id = None

        self.id = self._get_id()

        logger.info("Remind ({}) was created".format(self.id))


        except ValueError:
            try:
                self.when_remind = datetime.strptime(when_remind, '%Y/%m/%d %H:%M')
            except ValueError:
                try:
                    self.when_remind = datetime.strptime(when_remind, '%Y/%m/%d')
                except ValueError:
                    self.when_remind = datetime.now()

        if repeatable_time is not None:
            self.repeating_remind_relativedelta, self.is_repeatable = Remind._get_repeating_remind_relativedelta(
                repeatable_time)
        logger.info("Remind ({}) was updated".format(self.id))
